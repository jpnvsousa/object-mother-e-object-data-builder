public class UserBuilder {
	private User user = new User();
    public static final String DEFAULT_NAME = "John Smith";
    public static final String DEFAULT_ROLE = "ROLE_USER";
    public static final String DEFAULT_PASSWORD = "42";

    private String username;
    private String password = DEFAULT_PASSWORD;
    private String role = DEFAULT_ROLE;
    private String name = DEFAULT_NAME;

    private UserBuilder() {
    }

    public static UserBuilder aUser() {
        return new UserBuilder();
    }

    public UserBuilder withName(String name) {
    	user.setName(name);
        return this;
    }

    public UserBuilder withUsername(String username) {
        user.setUsername(username);
        return this;
    }

    public UserBuilder withPassword(String password) {
        user.setPassword(password);
        return this;
    }

    public UserBuilder withNoPassword() {
       user.setPassword(null);
        return this;
    }

    public UserBuilder inUserRole() {
    	user.setRole("ROLE_USER");
        return this;
    }

    public UserBuilder inAdminRole() {
    	user.setRole("ROLE_ADMIN");
        return this;
    }

    public UserBuilder inRole(String role) {
    	user.setRole(role);
        return this;
    }

    public UserBuilder but() {
        return UserBuilder
                .aUser()
                .inRole(role)
                .withName(name)
                .withPassword(password)
                .withUsername(username);
    }

    public User build() {
        return user;
    }
}