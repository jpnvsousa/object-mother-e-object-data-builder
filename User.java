class User {
	private String name;
	private String username;
	private String password;
	private String role;
	
	public User(String nome, String username, String password, String role) {
		this.name = nome;
		this.username = username;
		this.password = password;
		this.role = role;
	}
	public User() {
		// TODO Auto-generated constructor stub
	}
	public String getUsername() {
		return username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
